package cn.community.dto;

/**
 * @BelongsProject: community
 * @Author: XieGe
 * @CreateTime: 2019-07-19 00:02
 * @Description:
 */
public class GithubUser {
    private String name;
    private Long id;
    private String bio;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GithubUser").append('[')
                .append("name=")
                .append(name)
                .append(",id=")
                .append(id)
                .append(",bio=")
                .append(bio)
                .append(']');
        return sb.toString();
    }
}
